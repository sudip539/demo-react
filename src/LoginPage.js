import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';

import {createStackNavigator} from 'react-navigation-stack';
// const qs=require('qs')
import {
    AppRegistry,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ToastAndroid,
  TextInput,
  Button,
  TouchableOpacity,
  Linking,
  Alert,
  
  ImageBackground
} from 'react-native'
// import Profile from './ProfileScreen'



login = async() =>{
        
  const { email,password }  = this.state ;
  // alert(email);
  // alert(password);
 // ToastAndroid.show("clicked",ToastAndroid.SHORT)
  let data={email:email,password:password}
  fetch('https://staging.esolzbackoffice.com/ezy_care/web/app/login', {
      method: 'POST',
      // headers: {
      //   'Accept': 'application/json',
      //   'Content-Type': 'application/x-www-form-urlencoded',
      // },
      body: qs.stringify(data)
  })
  .then((response) => response.json())
  .then(async (res) => {
      if(res.status === true ){
          await AsyncStorage.setItem("userDetail", JSON.stringify(res.user_details));
          this.props.navigation.navigate('Profile', { userDetail: res.user_details });
          // this.props.navigation.navigate('Home');
          ToastAndroid.show("clicked",ToastAndroid.SHORT)

      }
      else{
          alert(res.message);
      }
  })
  .done();
}

export  class LoginPage extends Component  {
    state = {
        username: null,
        password: null
      }
      constructor(props) {
        super(props);
    }
      showData = async()=>{
       // let loginDetails = await AsyncStorage.getItem('loginDetails');
       // let ld = JSON.parse(loginDetails);
       // navigate('Profile', {name: 'Jane'})
       // alert('email: '+ ld.email + ' ' + 'password: ' + ld.password);
      //  ToastAndroid.show("clicked",ToastAndroid.SHORT)
    }
      _onSubmit = async() => {
        const { username, password } = this.state;
       // this.login
        alert('email: '+ username + ' ' + 'password: ' +password);
       // Alert.alert('Button has been pressed!');
       //this.props.navigation.navigate('Profile')
   //    ToastAndroid.show("clicked",ToastAndroid.SHORT)

       // ToastAndroid.show("clicked",ToastAndroid.SHORT)
            let data={email:username,password:password,device_token:"ab"}
      fetch('https://staging.esolzbackoffice.com/ezy_care/web/app/login', {
       method: 'POST',
       
       headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
}),
       
         body : "email=recep@gmail.com&password=123&device_token=ab"
     })
     .catch((error) => {
      console.error(error);
    })
    .then((response) => response.json())
    .then(async (res) => {
     // alert("test"+res.error);
     console.log(res);
       if(res.status === true ){
         //  await AsyncStorage.setItem("userDetail", JSON.stringify(res.user_details));
         //  this.props.navigation.navigate('Profile', { userDetail: res.user_details });
            this.props.navigation.navigate('MainStack');
           ToastAndroid.show("clicked",ToastAndroid.SHORT)
 
       }
       else{
          // alert(res.message);
           ToastAndroid.show("error",ToastAndroid.SHORT)
       }
   })

   .done();
       
      }
      
    render(){
     // const {navigate} =this.props.navigation
        return(
         

         <ImageBackground source={require('../assets/BG.png')} style={styles.backgroundImage} >
            <View  >
    <View style={{ marginTop: 60, flexDirection: 'row',alignItems: 'center',justifyContent:"center" }}>

       <View  style={  styles.container    }>
         <ImageBackground  source={require('../assets/logo.png')} style={styles.backgroundLogoImage} >
        <Text style={styles.welcome}>
          
        </Text>
        </ImageBackground>
       </View>
       </View>
       <View  style={  styles.container1    }>
        <Text style={styles.welcome1}>
          Login to Your Account
        </Text>
       </View>
       <View  style={  styles.usercontainer    }>
        <Text style={styles.username}>
           User Name
        </Text>
       </View>
       <View  style={  styles.edittext    }>
        <TextInput 
         inlineImageLeft='user'
         inlineImagePadding={34}
        onChangeText={(username) => this.setState({username})}
      >
        </TextInput>
       </View>
       <View  style={  styles.passcontainer    }>
        <Text style={styles.username}>
          
           Password
        </Text>
       </View>
       <View  style={  styles.passwordedittext    }>
        <TextInput 
        inlineImageLeft='passlock'
        inlineImagePadding={34}
          onChangeText={(password) => this.setState({password})}
      >
 

        </TextInput>
        </View>
        <View style={{ marginTop: 60, flexDirection: 'row',alignItems: 'center',justifyContent:"center" }}>
        <View style={{ height: 53, width: 340, }}>
      <TouchableOpacity onPress = {this._onSubmit}
      
       style={styles.buttoncontainer}
      >
        <ImageBackground  source={require('../assets/buttonlog.png')} style={styles.backgroundbuttonImage}>
    {/* <View style = {styles.buttoncontainer } > */}
    <View style={{ alignItems: 'center',justifyContent:"center" }}>
        <Text style = {styles.buttontextstyle}>Log in</Text>
        </View>
        </ImageBackground>
    {/* </View> */}
</TouchableOpacity>
</View>
</View>
      </View>
      </ImageBackground>
      
        )
    }
    
}
const styles = StyleSheet.create({
    container:{
      flex:0,
      width:180,
      height:145,
     
     
      justifyContent:"center",
      alignItems:"center"
      
    },
    
    usercontainer:{
     flex:0,
     height:25,
     marginTop:40,
     marginStart:20
    
    
   },
    container1:{
     flex:0,
     height:49,
     marginTop:10,
    
   
     justifyContent:"center",
     alignItems:"center"
     
   },
     welcome1:{
         fontSize:22,
         textAlign:"center",
         color:"#000000",
         fontStyle:"italic"
     },
     welcome:{
       fontSize:25,
       textAlign:"center",
       color:"#000000",
      
   },
   username:{
     fontSize:15,
     color:"#000000",
    
   },
   edittext:{
     flex:0,
     height: 50, 
     paddingStart:20,
     borderColor: 'black',
     borderRadius:25,
      borderWidth: 1,
      marginStart:20,
     
     marginTop:4,
     marginEnd:20,
     fontSize:45,
     color:"#1c2236"
   },
   passwordedittext:{
     flex:0,
     height: 50, 
     paddingStart:20,
     borderColor: 'black',
      borderWidth: 1,
      marginStart:20,
    borderRadius:25,
     marginTop:4,
     marginEnd:20,
     fontSize:45,
     color:"#1c2236"
   },
   passcontainer:{
     flex:0,
     height:25,
     marginTop:20,
     marginStart:20
    
     
     
     
   },
   buttoncontainer:{
     justifyContent:'center',
  
     
     height:'100%',
     height:'100%' ,
    
     
     
     
     
   },
   buttontextstyle:{
     marginTop:12,
     color:'white',
     fontSize:20,
     alignItems:"center"
     ,justifyContent:"center"
   },
   
   
   backgroundImage: {
     flex: 1,
     resizeMode: 'cover', // or 'stretch'
     width: '100%',
      height: '100%',
     
      //  resizeMode: Image.resizeMode.contain,
   
  },
  backgroundLogoImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
    width: '100%',
     height: '100%',
     justifyContent:"center",
     alignItems:"center"
    
     //  resizeMode: Image.resizeMode.contain,
  
 },
  backgroundbuttonImage: {
    flex: 0,
    resizeMode: 'cover', // or 'stretch'
    width: '100%',
     height: '100%'
     
    
     //  resizeMode: Image.resizeMode.contain,
  
 }
   },
  
   
   
   
   );

