import React, { Component } from "react";
// import LoginPage from '.src/LoginPage'
import DrawerContent from './DrawerContent';
import { LoginPage } from './LoginPage';
import { Provider } from 'react-redux';
import Profile from './ProfileScreen'
// import {DashboardScreen }from './Dashboard'
import HomeScreen from './HomeScreen';
import RankingScreen from './RankingScreen';
import { View, TouchableOpacity, TextInput, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createStackNavigator } from 'react-navigation-stack'
import { createDrawerNavigator } from 'react-navigation-drawer'
import { createStore, combineReducers, applyMiddleware } from 'redux';
import {
    createAppContainer,
    
    createSwitchNavigator,
       createBottomTabNavigator,
    createMaterialTopTabNavigator
} from "react-navigation";

const DrawerNavigation = createDrawerNavigator(
  {
    HomeScreen: { screen: HomeScreen },
    RankingScreen: { screen: RankingScreen },
  },
  {
    contentComponent: DrawerContent,
    drawerPosition: 'left',
    drawerWidth: 250,
    navigationOptions: { header: null }
  }
);
const MainStack = createStackNavigator(
  {
    DrawerNavigation: DrawerNavigation,
    
    
   
  },


  {
    navigationOptions: navigator => ({
      // headerLeft: (
      //   <TouchableOpacity
      //     onPress={() => {
      //       navigator.navigation.toggleDrawer();
      //     }}
      //   >
      //     <Icon
          
      //       name="bars"
      //       size={20}
      //       color="#aaa"
      //       style={styles.headerLeftIconStyle}
      //     />
      //   </TouchableOpacity>
      // ),
      // headerTitle: (
      //   <View style={styles.searchInputContainer}>
      //     <Icon
      //       name="search"
      //       size={20}
      //       color="#aaa"
      //       style={styles.searchInputIconStyle}
      //     />
      //     <TextInput
      //       style={styles.searchInputStyle}
      //       underlineColorAndroid="transparent"
      //       placeholder="search"
      //     />
      //   </View>
      // ),
      headerMode:'none',
      drawerLockMode: 'locked-open',
    }),
  }
);



const AuthStack = createStackNavigator({
  LoginPage: {
      screen: LoginPage
  },
  Profile:{
    screen: Profile
  },
  MainStack:{
    screen: MainStack
  }
  
},
  {
      headerMode: "none",
     // initialRouteName:  'LoginPage',
    transitionConfig: TransitionConfig,
  }
);


let Navigation = createAppContainer(
  createSwitchNavigator({
      Auth: {
          screen: AuthStack
      },

     
     
  })
);
const styles = StyleSheet.create({
  headerLeftIconStyle: {
    marginLeft: 15,
  },
  searchInputContainer: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#999',
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  searchInputIconStyle: {
    padding: 5,
  },
  searchInputStyle: {
    flex: 1,
    paddingRight: 10,
    textAlign: 'left',
  },
});

let TransitionConfig = () => {
  return {
    screenInterpolator: ({ position, scene }) => {
      const opacity = position.interpolate({
        inputRange: [scene.index - 1, scene.index],
        outputRange: [0, 1],
      });
      return {
        opacity: opacity,
      };
    },
  };
};
export default class App extends React.Component {
  render() {
      return (
          // <Provider store={store}>
              <Navigation />
          // </Provider>
      );
  }
}