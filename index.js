/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import { LoginPage } from './src/LoginPage';
// import { Profile } from './src/ProfileScreen';
 import App from './src/Route';

// export const AppNavigator = StackNavigator(Auth)
const reactNavigationSample = props => {
    return <App navigation={props.navigation} />;
  };
  
//   reactNavigationSample.navigationOptions = {
//     title: "Home Screen"
//   };
//   const SimpleApp = createStackNavigator({
//     Home: { screen: reactNavigationSample },
//     LoginPage: { screen: LoginPage, title: "Second Screen" }
//   });
  
  
AppRegistry.registerComponent(appName, () => App );
