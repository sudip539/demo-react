/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';

import {
  AppRegistry,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  TouchableOpacity,
  Linking
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export class LoginPage extends Component {
render(){
  return (
   
    <View
          >
       <View  style={  styles.container    }>
        <Text style={styles.welcome}>
          Demoexport
        </Text>
       </View>
       <View  style={  styles.container1    }>
        <Text style={styles.welcome1}>
          Login
        </Text>
       </View>
       <View  style={  styles.usercontainer    }>
        <Text style={styles.username}>
           User Name
        </Text>
       </View>
       <View  style={  styles.edittext    }>
        <TextInput 
        
      >
        </TextInput>
       </View>
       <View  style={  styles.passcontainer    }>
        <Text style={styles.username}>
           Password
        </Text>
       </View>
       <View  style={  styles.passwordedittext    }>
        <TextInput 
      >
        </TextInput>
        </View>

      <TouchableOpacity onPress = {() => {this._onSubmit}}>
    <View style = {styles.buttoncontainer }
           >
        <Text style = {styles.buttontextstyle}>Log in</Text>
    </View>
</TouchableOpacity>
      </View>
      
      
  );
}
  
};

const styles = StyleSheet.create({
 container:{
   flex:0,
   height:49,
  
   backgroundColor:"#092be8",
   justifyContent:"center",
   alignItems:"center"
   
 },
 usercontainer:{
  flex:0,
  height:25,
  marginTop:40,
  marginStart:20
 
  
  
  
},
 container1:{
  flex:0,
  height:49,
  marginTop:100,
 

  justifyContent:"center",
  alignItems:"center"
  
},
  welcome1:{
      fontSize:25,
      textAlign:"center",
      color:"#000000",
      fontStyle:"italic"
  },
  welcome:{
    fontSize:25,
    textAlign:"center",
    color:"#ffffff",
   
},
username:{
  fontSize:15,
  color:"#000000",
 
},
edittext:{
  flex:0,
  height: 40, 
  paddingStart:10,
  borderColor: 'gray',
   borderWidth: 1,
   marginStart:20,
  
  marginTop:4,
  marginEnd:20,
  fontSize:45,
  color:"#1c2236"
},
passwordedittext:{
  flex:0,
  height: 40, 
  paddingStart:10,
  borderColor: 'gray',
   borderWidth: 1,
   marginStart:20,
  
  marginTop:4,
  marginEnd:20,
  fontSize:45,
  color:"#1c2236"
},
passcontainer:{
  flex:0,
  height:25,
  marginTop:20,
  marginStart:20
 
  
  
  
},
buttoncontainer:{
  marginTop:100,
  marginHorizontal:80,
  backgroundColor: 'blue',
   alignItems: 'center',
  height:50, 
  justifyContent: 'center',
   borderRadius: 15
  
  
  
},
buttontextstyle:{
  color:'white',
  fontSize:20
}

});

AppRegistry.registerComponent('AwesomeProject', () => LoginPage);